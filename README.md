# keitetsu’s dotfiles

## License [![](http://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)][license]

Copyright © Oliver Stotzem

Licensed under the [MIT license][license].

[license]: LICENSE.md
